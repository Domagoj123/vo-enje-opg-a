#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Domagoj_Rogovski_projekt_head.h"
int main(void) {
	unsigned int izbornik = -1;
	char izbor[3] = { '\0' };
	while (1) {
        printf("|******************************************\
                \n             Dobrodosli!                 \
                \n Odaberite opciju koju zelite izvrsiti:  \
                \n\n1. Dodavanje novog proizvoda\
                \n2. Ispis podataka o proizvodima\
                \n3. Brisanje proizvoda iz datoteke\
                \n4. Brisanje svih podataka\
                \n5. Zavrsetak programa\
                \n******************************************|\n");
        scanf("%u", &izbornik);
        switch (izbornik) {
        case 1:
            stvaranjeDatoteke();
            unosPodataka();
            brojacPodataka();
            break;
        case 2:
            ispisPodataka();
            break;
        case 3:
            brisanjePodatka();
            break;
        case 4:
            brisanjeDatoteke();
            break;
        case 5:
            printf("Jeste li sigurni da zelite zavrsiti program?\n");
            printf("Da\tNe:  ");
            scanf("%2s", izbor);
            if (!strcmp("Da", izbor)) {
                exit(EXIT_FAILURE);
            }
            else {
                break;
            }
            break;
        default:
            printf("\nKrivo odabrana opcija, pokusajte ponovno!\n\n");
        }
	}
    return 0;
}