#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "Domagoj_Rogovski_projekt_head.h"


void stvaranjeDatoteke(void) {
	int podatak = 0;
	FILE* fp = NULL;
	fp = fopen("proizvodi.bin", "rb+");
	if (fp == NULL) {
		printf("\nDatoteka ne postoji!\n");
		FILE* fp1 = fopen("proizvodi.bin", "wb+");
		if (fp1 == NULL) {
			printf("\n");
			printf("\nDatoteku nije moguce napraviti!\n");
			printf("\n");
		}
		else {
			fwrite(&podatak, sizeof(int), 1, fp1);
			fclose(fp1);
			printf("\nDatoteka uspjesno kreirana.\n\n");
		}
	}
}


void unosPodataka(void) {
	PROIZVOD* noviProizvod = NULL;
	noviProizvod = (PROIZVOD*)calloc(1, sizeof(PROIZVOD));
	if (noviProizvod == NULL) {
		printf("\n");
		perror("Error");
		printf("\n");
	}
	FILE* fp = NULL;
	fp = fopen("proizvodi.bin", "ab+");
	if (fp == NULL) {
		printf("\n");
		perror("Otvaranje");
		printf("\n");
	}
	else {
		printf("Unesite ime proizvoda: ");
		scanf("%29s", noviProizvod->ime);
		printf("Unesite cijenu proizvoda: ");
		scanf("%f", &noviProizvod->cijena);
		printf("Unesite kolicinu proizvoda: ");
		scanf("%d", &noviProizvod->kolicina);
		fwrite(noviProizvod, sizeof(PROIZVOD), 1, fp);
		fclose(fp);
		printf("\nUspjesno spremljeni podaci.\n\n");
	}
	free(noviProizvod);
}


void brojacPodataka(void) {
	int counter = 0;
	FILE* fp = NULL;
	fp = fopen("proizvodi.bin", "rb+");
	if (fp == NULL) {
		printf("\n");
		perror("Otvaranje");
		printf("\n");
	}
	else {
		fread(&counter, sizeof(int), 1, fp);
		if (counter == 0) {
			counter = 1;
			fseek(fp, 0, SEEK_SET);
			fwrite(&counter, sizeof(int), 1, fp);
		}
		else {
			counter++;
			fseek(fp, 0, SEEK_SET);
			fwrite(&counter, sizeof(int), 1, fp);
		}
		fclose(fp);
	}
}


void ispisPodataka(void) {
	PROIZVOD* proizvodi = NULL;
	FILE* fp = NULL;
	fp = fopen("proizvodi.bin", "rb");
	if (fp == NULL) {
		printf("\n");
		perror("Otvaranje");
		printf("\n");
	}
	else {
		int counter;
		fread(&counter, sizeof(int), 1, fp);
		proizvodi = (PROIZVOD*)calloc(counter, sizeof(PROIZVOD));
		if (proizvodi == NULL) {
			printf("\n");
			perror("Error");
			printf("\n");
		}
		fread(proizvodi, sizeof(PROIZVOD), counter, fp);
		int number;
		printf("\n");
		do {
			printf("Odaberite kakav ispis zelite:\n");
			printf("1.Od najjeftinijeg do najskupljeg proizvoda\n");
			printf("2.Od najskupljeg do najjeftinijeg proizvoda\n");
			scanf("%d", &number);
		} while (number < 1 || number > 2);
		if (number == 1) {
			qsort(proizvodi, counter, sizeof(PROIZVOD), SortMin);
			for (int i = 0; i < counter; i++) {
				printf("\nIme %d. proizvoda: %s\nCijena %d. proizvoda: %.2fkn\nKolicina %d. proizvoda: %dkom\n\n",
					i + 1, (proizvodi + i)->ime, i + 1, (proizvodi + i)->cijena, i + 1, (proizvodi + i)->kolicina);
			}
		}
		if (number == 2) {
			qsort(proizvodi, counter, sizeof(PROIZVOD), SortMax);
			for (int i = 0; i < counter; i++) {
				printf("\nIme %d. proizvoda: %s\nCijena %d. proizvoda: %.2fkn\nKolicina %d. proizvoda: %dkom\n\n",
					i + 1, (proizvodi + i)->ime, i + 1, (proizvodi + i)->cijena, i + 1, (proizvodi + i)->kolicina);
			}
		}
		fclose(fp);
		free(proizvodi);
	}
}


int SortMin(const void* p, const void* q) {
	int l = ((struct proizvod*)p)->cijena;
	int r = ((struct proizvod*)q)->cijena;
	return (float)(l - r);
}


int SortMax(const void* p, const void* q) {
	int l = ((struct proizvod*)p)->cijena;
	int r = ((struct proizvod*)q)->cijena;
	return (float)(r - l);
}


void brisanjePodatka(void) {
	int counter;
	FILE* fp = NULL;
	fp = fopen("proizvodi.bin", "rb");
	if (fp == NULL) {
		printf("\n");
		perror("Otvaranje");
		printf("\n");
	}
	FILE* pomocni = NULL;
	pomocni = fopen("pomocnaDatoteka.bin", "wb");
	if (fp == NULL) {
		printf("\n");
		perror("Kreiranje");
		printf("\n");
	}
	int odabir;
	do {
		printf("Uklanjane proizvoda:\n1.Po imenu\n2.Povratak\n\n");
		scanf("%d", &odabir);
	} while (odabir < 1 || odabir > 2);
	switch (odabir) {
	case 1: {
		int new = 0;
		int found = 0;
		char* trazeni_proizvod = NULL;
		trazeni_proizvod = (char*)calloc(30, sizeof(char));
		if (trazeni_proizvod == NULL) {
			printf("\n");
			perror("Error");
			printf("\n");
		}
		fread(&counter, sizeof(int), 1, fp);
		PROIZVOD* proizvodi = NULL;
		proizvodi = (PROIZVOD*)calloc(counter, sizeof(PROIZVOD));
		if (proizvodi == NULL) {
			printf("\n");
			perror("Error");
			printf("\n");
		}
		printf("\nUnesite ime proizvoda kojeg zelite obrisati:\n");
		fflush(stdin);
		scanf("%29s", trazeni_proizvod);
		new = counter - 1;
		fwrite(&new, sizeof(int), 1, pomocni);
		for (int i = 0; i < counter; i++) {
			fread(proizvodi, sizeof(PROIZVOD), 1, fp);
			if (strcmp(proizvodi->ime, trazeni_proizvod) == 0) {
				found = 1;
			}
			else {
				fwrite(proizvodi, sizeof(PROIZVOD), 1, pomocni);
			}
		}
		if (found != 1) {
			printf("\nNe postoji takav proizvod!\n\n");
			fclose(fp);
			fclose(pomocni);
			free(proizvodi);
		}
		else {
			printf("\nProizvod obrisan!\n\n");
			fclose(fp);
			fclose(pomocni);
			free(proizvodi);
			remove("proizvodi.bin");
			rename("pomocnaDatoteka.bin", "proizvodi.bin");
			remove("pomocnaDatoteka.bin");
		}
		break;
	}
	case 2:
		break;
	}
}


void brisanjeDatoteke(void) {
	FILE* fp = NULL;
	char* ime_datoteke = "proizvodi.bin";
	int status = 0;
	fp = fopen(ime_datoteke, "rb");
	if (fp == NULL) {
		printf("\nDatoteka %s ne postoji na disku.\n\n", ime_datoteke);
		status = -1;
	}
	else {
		fclose(fp);
	}
	status = remove(ime_datoteke);
	if (status == 0) {
		printf("\nUspjesno obrisana datoteka.\n\n");
	}
	else {
		printf("\nNemogucnost brisanja datoteke!\n\n");
	}
}